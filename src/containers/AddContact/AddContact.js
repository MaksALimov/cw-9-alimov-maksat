import React, {useState} from 'react';
import './AddContact.css';
import {sendContact} from "../../store/actions/addContactActions";
import {useDispatch} from "react-redux";

const AddContact = ({history}) => {
    const dispatch = useDispatch();

    const [userData, setUserData] = useState({
        name: '',
        phone: '',
        email: '',
        photo: '',
    });

    const onInputChange = e => {
        const {name, value} = e.target;

        setUserData(prevState => ({...prevState, [name]: value}));
    };

    const onSubmit = async e => {
        e.preventDefault();
        await dispatch(sendContact(userData));
        history.replace('/');
    }

    const getBack = () => {
        history.replace('/');
    }

    return (
       <>
           <h2 className="AddNewContactTitle">Add new contact</h2>
           <form className="AddContactForm" onSubmit={onSubmit}>
               <input
                   type="text"
                   name="name"
                   placeholder="Введите имя"
                   value={userData.name}
                   onChange={onInputChange}
               />
               <input
                   type="tel"
                   name="phone"
                   placeholder="Введите номер"
                   value={userData.phone}
                   onChange={onInputChange}
               />
               <input
                   type="email"
                   name="email"
                   placeholder="Введите e-mail"
                   value={userData.email}
                   onChange={onInputChange}
               />
               <input
                   type="text"
                   name="photo"
                   placeholder="Ссылка на картинку"
                   value={userData.photo}
                   onChange={onInputChange}
               />
               {
                   userData.photo ?
                   <div className="PhotoPreviewWrapper">
                       <span className="PhotoPreview__text">Photo Preview</span>
                       <img src={userData.photo} alt="userPhoto"/>
                   </div> : null
               }
                   <button type="Submit" onClick={onSubmit}>Save</button>
                   <button onClick={getBack}>Back to contact</button>
           </form>
       </>
    );
};

export default AddContact;