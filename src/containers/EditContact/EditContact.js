import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {
    editContacts,
    onInputEmailChange, onInputImageChange,
    onInputNameChange,
    onInputPhoneChange
} from "../../store/actions/editContactActions";

const EditContact = ({history}) => {
    const dispatch = useDispatch();
    const currentContactId = useSelector(state => state.currentContact.currentContactId)
    const name = useSelector(state => state.editContact.name)
    const phone = useSelector(state => state.editContact.phone)
    const email = useSelector(state => state.editContact.email)
    const photo = useSelector(state => state.editContact.photo)

    const onSubmit = async e => {
        e.preventDefault();
        await dispatch(editContacts(currentContactId, name, phone, email, photo));
        history.replace('/');
    }

    const getBack = () => {
        history.replace('/');
    }

    const changeName = name => {
        dispatch(onInputNameChange(name));
    };

    const changePhone = phone => {
        dispatch(onInputPhoneChange(phone));
    };

    const changeEmail = email => {
        dispatch(onInputEmailChange(email));
    };

    const changeImage = image => {
        dispatch(onInputImageChange(image));
    };


    return (
        <>
            <h2 className="AddNewContactTitle">Edit Contact</h2>
            <form className="AddContactForm" onSubmit={onSubmit}>
                <input
                    type="text"
                    name="name"
                    placeholder="Введите имя"
                    value={name}
                    onChange={e => changeName(e.target.value)}
                />
                <input
                    type="tel"
                    name="phone"
                    placeholder="Введите номер"
                    value={phone}
                    onChange={e => changePhone(e.target.value)}
                />
                <input
                    type="email"
                    name="email"
                    placeholder="Введите e-mail"
                    value={email}
                    onChange={e => changeEmail(e.target.value)}
                />
                <input
                    type="text"
                    name="photo"
                    placeholder="Ссылка на картинку"
                    value={photo}
                    onChange={e => changeImage(e.target.value)}
                />
                {
                    photo ?
                        <div className="PhotoPreviewWrapper">
                            <span className="PhotoPreview__text">Photo Preview</span>
                            <img src={photo} alt="userPhoto"/>
                        </div> : null
                }
                <button type="Submit" onClick={onSubmit}>Save</button>
                <button onClick={getBack}>Back to contact</button>
            </form>
        </>
    );
};

export default EditContact;