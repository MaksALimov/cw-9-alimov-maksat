import {
    GET_CONTACTS_LIST_FAILURE,
    GET_CONTACTS_LIST_REQUEST,
    GET_CONTACTS_LIST_SUCCESS
} from "../actions/contactsListActions";

const initialState = {
    error: null,
    contactsList: [],
};

const contactsListReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_CONTACTS_LIST_REQUEST: {
            return {...state, error: null};
        }

        case GET_CONTACTS_LIST_SUCCESS: {
            return {...state, contactsList: action.payload, error: false};
        }

        case GET_CONTACTS_LIST_FAILURE: {
            return {...state, error: action.payload};
        }

        default:
            return state;
    }
};

export default contactsListReducer;