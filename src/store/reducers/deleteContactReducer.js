import {DELETE_CONTACT_FAILURE, DELETE_CONTACT_REQUEST, DELETE_CONTACT_SUCCESS} from "../actions/deleteContactActions";

const initialState = {
    error: null,
};

const deleteContactReducer = (state = initialState, action) => {
    switch (action.type) {
        case DELETE_CONTACT_REQUEST: {
            return {...state, error: null};
        }

        case DELETE_CONTACT_SUCCESS: {
            return {...state, error: false};
        }

        case DELETE_CONTACT_FAILURE: {
            return {...state, error: action.payload};
        }

        default:
            return state;
    }
};

export default deleteContactReducer;