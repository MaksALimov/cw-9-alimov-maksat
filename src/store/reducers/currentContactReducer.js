import {
    GET_CURRENT_CONTACT_FAILURE,
    GET_CURRENT_CONTACT_REQUEST,
    GET_CURRENT_CONTACT_SUCCESS, SAVE_CURRENT_CONTACT_ID, SHOW_MODAL
} from "../actions/currentContactActions";

const initialState = {
    error: null,
    currentContact: null,
    currentContactId: null,
};

const currentContactReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_CURRENT_CONTACT_REQUEST: {
            return {...state, error: null};
        }

        case GET_CURRENT_CONTACT_SUCCESS: {
            return {...state, currentContact: action.payload};
        }

        case GET_CURRENT_CONTACT_FAILURE: {
            return {...state, error: action.payload};
        }

        case SHOW_MODAL: {
            return {...state, currentContact: null};
        }

        case SAVE_CURRENT_CONTACT_ID: {
            return {...state, currentContactId: action.payload};
        }

        default:
            return state;
    }
};

export default currentContactReducer;
