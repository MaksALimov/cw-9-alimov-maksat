import {
    CHANGEABLE_CONTACT_DATA_FAILURE,
    CHANGEABLE_CONTACT_DATA_REQUEST, CHANGEABLE_CONTACT_DATA_SUCCESS,
    EDIT_CONTACT_FAILURE,
    EDIT_CONTACT_REQUEST,
    EDIT_CONTACT_SUCCESS, ON_INPUT_EMAIL_CHANGE, ON_INPUT_IMAGE_CHANGE, ON_INPUT_NAME_CHANGE, ON_INPUT_PHONE_CHANGE
} from "../actions/editContactActions";

const initialState = {
    error: null,
    name: '',
    phone: '',
    photo: '',
    email: '',
};

const editContactReducer = (state = initialState, action) => {
    switch (action.type) {
        case EDIT_CONTACT_REQUEST: {
            return {...state, error: null};
        }

        case EDIT_CONTACT_SUCCESS: {
            return {...state, error: false};
        }

        case EDIT_CONTACT_FAILURE: {
            return {...state, error: action.payload};
        }

        case CHANGEABLE_CONTACT_DATA_REQUEST: {
            return {...state, error: null};
        }

        case CHANGEABLE_CONTACT_DATA_SUCCESS: {
            return {...state, name: action.payload.name, phone: action.payload.phone, email: action.payload.email, photo: action.payload.photo};
        }

        case CHANGEABLE_CONTACT_DATA_FAILURE: {
            return {...state, error: action.payload};
        }

        case ON_INPUT_NAME_CHANGE: {
            return {...state, name: action.payload};
        }

        case ON_INPUT_PHONE_CHANGE: {
            return {...state, phone: action.payload};
        }

        case ON_INPUT_EMAIL_CHANGE: {
            return {...state, email: action.payload};
        }

        case ON_INPUT_IMAGE_CHANGE: {
            return {...state, photo: action.payload};
        }

        default:
            return state;
    }
};

export default editContactReducer;