import {ADD_CONTACT_FAILURE, ADD_CONTACT_REQUEST, ADD_CONTACT_SUCCESS} from "../actions/addContactActions";

const initialState = {
    error: null,
};

const addContactReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_CONTACT_REQUEST: {
            return {...state, error: null};
        }

        case ADD_CONTACT_SUCCESS: {
            return {...state, error: false};
        }

        case ADD_CONTACT_FAILURE: {
            return {...state, error: action.payload};
        }

        default:
            return state;
    }
};

export default addContactReducer;