import axiosApi from "../../axiosApi";

export const DELETE_CONTACT_REQUEST = 'DELETE_CONTACT_REQUEST';
export const DELETE_CONTACT_SUCCESS = 'DELETE_CONTACT_SUCCESS';
export const DELETE_CONTACT_FAILURE = 'DELETE_CONTACT_FAILURE';

export const deleteContactRequest = () => ({type: DELETE_CONTACT_REQUEST});
export const deleteContactSuccess = () => ({type: DELETE_CONTACT_SUCCESS});
export const deleteContactsFailure = error => ({type: DELETE_CONTACT_FAILURE, payload: error});

export const deleteContact = (contactId, updatePage) => {
    return async dispatch => {
        try {
            dispatch(deleteContactRequest());

            await axiosApi.delete(`/contacts/${contactId}.json`);
            dispatch(deleteContactSuccess());
            updatePage();
        } catch (error) {
            dispatch(deleteContactsFailure(error));
        }
    };
};