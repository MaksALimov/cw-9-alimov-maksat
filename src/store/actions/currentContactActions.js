import axiosApi from "../../axiosApi";

export const GET_CURRENT_CONTACT_REQUEST = 'GET_CURRENT_CONTACT_REQUEST';
export const GET_CURRENT_CONTACT_SUCCESS = 'GET_CURRENT_CONTACT_SUCCESS';
export const GET_CURRENT_CONTACT_FAILURE = 'GET_CURRENT_CONTACT_FAILURE';
export const SHOW_MODAL = 'SHOW_MODAL';
export const SAVE_CURRENT_CONTACT_ID = 'SAVE_CURRENT_CONTACT_ID';

export const getCurrentContactRequest = () => ({type: GET_CURRENT_CONTACT_REQUEST});
export const getCurrentContactSuccess = currentContact => ({type: GET_CURRENT_CONTACT_SUCCESS, payload: currentContact});
export const getCurrentContactFailure = error => ({type: GET_CURRENT_CONTACT_FAILURE, payload: error});
export const showModal = () => ({type: SHOW_MODAL});
export const saveCurrentContactId = currentContactId => ({type: SAVE_CURRENT_CONTACT_ID, payload: currentContactId});

export const getCurrentContact = contactId => {
    return async dispatch => {
        try {
            dispatch(getCurrentContactRequest());

            const response = await axiosApi.get(`/contacts/${contactId}.json`);
            dispatch(getCurrentContactSuccess(response.data));
            dispatch(saveCurrentContactId(contactId));
        } catch (error) {
            dispatch(getCurrentContactFailure(error));
        }
    };
};