import axiosApi from "../../axiosApi";

export const ADD_CONTACT_REQUEST = 'ADD_CONTACT_REQUEST';
export const ADD_CONTACT_SUCCESS = 'ADD_CONTACT_SUCCESS';
export const ADD_CONTACT_FAILURE = 'ADD_CONTACT_FAILURE';

export const addContactRequest = () => ({type: ADD_CONTACT_REQUEST});
export const addContactSuccess = () => ({type: ADD_CONTACT_SUCCESS});
export const addContactFailure = error => ({type: ADD_CONTACT_FAILURE, payload: error});

export const sendContact = contactData => {
    return async dispatch => {
        try {
            dispatch(addContactRequest());

            await axiosApi.post('/contacts.json', contactData);

            dispatch(addContactSuccess());
        } catch (error) {
            dispatch(addContactFailure(error));
        }
    };
};