import axiosApi from "../../axiosApi";

export const EDIT_CONTACT_REQUEST = 'EDIT_CONTACT_REQUEST';
export const EDIT_CONTACT_SUCCESS = 'EDIT_CONTACT_SUCCESS';
export const EDIT_CONTACT_FAILURE = 'EDIT_CONTACT_FAILURE';

export const ON_INPUT_NAME_CHANGE = 'ON_INPUT_NAME_CHANGE';
export const ON_INPUT_PHONE_CHANGE = 'ON_INPUT_PHONE_CHANGE';
export const ON_INPUT_IMAGE_CHANGE = 'ON_INPUT_IMAGE_CHANGE';
export const ON_INPUT_EMAIL_CHANGE = 'ON_INPUT_EMAIL_CHANGE';

export const CHANGEABLE_CONTACT_DATA_REQUEST = 'GET_CONTACT_DATA'
export const CHANGEABLE_CONTACT_DATA_SUCCESS = 'CHANGEABLE_CONTACT_DATA_SUCCESS';
export const CHANGEABLE_CONTACT_DATA_FAILURE = 'CHANGEABLE_CONTACT_DATA_FAILURE';

export const editContactRequest = () => ({type: EDIT_CONTACT_REQUEST});
export const editContactSuccess = () => ({type: EDIT_CONTACT_SUCCESS});
export const editContactFailure = error => ({type: EDIT_CONTACT_FAILURE, payload: error});

export const onInputNameChange = name => ({type: ON_INPUT_NAME_CHANGE, payload: name});
export const onInputPhoneChange = phone => ({type: ON_INPUT_PHONE_CHANGE, payload: phone});
export const onInputImageChange = image => ({type: ON_INPUT_IMAGE_CHANGE, payload: image});
export const onInputEmailChange = email => ({type: ON_INPUT_EMAIL_CHANGE, payload: email});

export const changeableContactDataRequest = () => ({type: CHANGEABLE_CONTACT_DATA_REQUEST});
export const changeableContactDataSuccess = contactData => ({type: CHANGEABLE_CONTACT_DATA_SUCCESS, payload: contactData});
export const changeableContactDataFailure = error => ({type: CHANGEABLE_CONTACT_DATA_FAILURE, error: error});


export const getChangeableContactData = contactId => {
    return async dispatch => {
        try {
            dispatch(changeableContactDataRequest());

            const response = await axiosApi.get(`/contacts/${contactId}.json`);
            dispatch(changeableContactDataSuccess(response.data));
        } catch (error) {
            dispatch(changeableContactDataFailure(error));
        }
    };
};

export const editContacts = (contactId, name, phone, email, photo) => {
    return async dispatch => {
        try {
            dispatch(editContactRequest());

            await axiosApi.put(`/contacts/${contactId}.json`, {name, phone, email, photo});
            dispatch(editContactSuccess());

        } catch (error) {
            dispatch(editContactFailure(error));
        }
    };
};