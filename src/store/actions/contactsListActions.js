import axiosApi from "../../axiosApi";

export const GET_CONTACTS_LIST_REQUEST = 'GET_CONTACTS_LIST_REQUEST';
export const GET_CONTACTS_LIST_SUCCESS = 'GET_CONTACTS_LIST_SUCCESS';
export const GET_CONTACTS_LIST_FAILURE = 'GET_CONTACTS_LIST_FAILURE';

export const getContactsListRequest = () => ({type: GET_CONTACTS_LIST_REQUEST});
export const getContactsListSuccess = contactsList => ({type: GET_CONTACTS_LIST_SUCCESS, payload: contactsList});
export const getContactsListFailure = error => ({type: GET_CONTACTS_LIST_FAILURE, payload: error});

export const getContactsList = () => {
    return async dispatch => {
        try {
            dispatch(getContactsListRequest());

            const response = await axiosApi.get('/contacts.json');
            dispatch(getContactsListSuccess(response.data));
        } catch (error) {
            dispatch(getContactsListFailure(error));
        }
    };
};