import React from 'react';
import './Header.css';
import {Link} from "react-router-dom";

const Header = () => {
    return (
        <>
            <div className="Header">
                <Link to="/">
                    <h1 className="Header__title">Contacts</h1>
                </Link>
                <Link to="/add-contact">
                    <button className="Header__contacts-btn">Add new Contact</button>
                </Link>
            </div>
            <div className="Line"/>
        </>
    );
};

export default Header;