import React, {useCallback} from 'react';
import './Modal.css';
import {useDispatch, useSelector} from "react-redux";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faWindowClose} from "@fortawesome/free-solid-svg-icons";
import {faPhoneAlt} from "@fortawesome/free-solid-svg-icons";
import {faEnvelope} from "@fortawesome/free-solid-svg-icons";
import {faTrash} from "@fortawesome/free-solid-svg-icons";
import {faEdit} from "@fortawesome/free-solid-svg-icons";
import {Link} from "react-router-dom";
import {getChangeableContactData} from "../../../store/actions/editContactActions";
import {deleteContact} from "../../../store/actions/deleteContactActions";
import {getContactsList} from "../../../store/actions/contactsListActions";

const Modal = () => {
    const dispatch = useDispatch();
    const currentContact = useSelector(state => state.currentContact.currentContact);
    const currentContactId = useSelector(state => state.currentContact.currentContactId);

    const updatePage = useCallback(() => {
        dispatch(getContactsList());
    }, [dispatch]);

    return (
        <>
            <div className="ModalWrapper">
                <div className="CloseModal"><FontAwesomeIcon icon={faWindowClose}/></div>
                <div>
                    <img src={currentContact.photo} alt=""/>
                </div>
                <div>
                    <h2 className="ModalWrapper__title-name">{currentContact.name}</h2>
                    <a href={`tel:${currentContact.phone}`} className="ModalWrapper__links">
                        <span className="ModalWrapper__icons">
                            <FontAwesomeIcon icon={faPhoneAlt}/>
                        </span>
                        {currentContact.phone}
                    </a>
                    <a href={`mailto:${currentContact.email}`} className="ModalWrapper__links">
                        <span className="ModalWrapper__icons">
                            <FontAwesomeIcon icon={faEnvelope}/>
                        </span>
                        {currentContact.email}
                    </a>
                    <div>
                        <Link to="/edit-contact">
                            <button onClick={() => dispatch(getChangeableContactData(currentContactId))}
                                    className="ModalWrapper__edit-btn"
                            >
                                <span className="ModalWrapper__icons"><FontAwesomeIcon icon={faEdit}/></span>
                                Edit
                            </button>
                        </Link>
                        <button onClick={() => dispatch(deleteContact(currentContactId, updatePage))}
                                className="ModalWrapper__delete-btn"
                        >
                            <span className="ModalWrapper__icons"><FontAwesomeIcon icon={faTrash}/></span>
                            Delete
                        </button>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Modal;