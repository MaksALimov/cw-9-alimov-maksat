import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getContactsList} from "../../store/actions/contactsListActions";
import './ContactsList.css';
import {getCurrentContact, showModal} from "../../store/actions/currentContactActions";
import Modal from "../UI/Modal/Modal";
import Backdrop from "../UI/Backdrop/Backdrop";

const ContactsList = () => {
    const dispatch = useDispatch();
    const contactsList = useSelector(state => state.contactsList.contactsList);
    const currentContact = useSelector(state => state.currentContact.currentContact);

    useEffect(() => {
        dispatch(getContactsList());
    }, [dispatch]);

    const getContactInfo = contactId => {
        dispatch(getCurrentContact(contactId));
    };

    return (
        <>
            {Object.keys(contactsList).map(contact => (
                <div key={contact} className="ContactsListWrapper" onClick={() => dispatch(showModal())}>
                    <h4
                        className="ContactsListWrapper__title-name"
                        onClick={() => getContactInfo(contact)}
                    >
                        {contactsList[contact].name}
                    </h4>
                    <img className="ContactsListWrapper__img" src={contactsList[contact].photo} alt="person"/>
                    {currentContact ? <> <Modal/><Backdrop/> </> : null}
                </div>
            ))}
        </>
    );
};

export default ContactsList;