import axios from "axios";

const axiosApi = axios.create({
    baseURL: 'https://contacts-list-js-11-default-rtdb.firebaseio.com'
});

export default axiosApi;