import Header from "./components/Header/Header";
import {Route} from "react-router-dom";
import AddContact from "./containers/AddContact/AddContact";
import ContactsList from "./components/ContactsList/ContactsList";
import EditContact from "./containers/EditContact/EditContact";

const App = () => (
    <>
        <Header/>
        <Route path="/" exact component={ContactsList}/>
        <Route path="/add-contact" component={AddContact}/>
        <Route path="/edit-contact" component={EditContact}/>
    </>
);

export default App;
