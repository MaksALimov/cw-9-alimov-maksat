import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {createStore, applyMiddleware, compose, combineReducers} from "redux";
import {Provider} from "react-redux";
import thunk from "redux-thunk";
import {BrowserRouter} from "react-router-dom";
import addContactReducer from "./store/reducers/addContactReducer";
import contactsListReducer from "./store/reducers/contactsListReducer";
import currentContactReducer from "./store/reducers/currentContactReducer";
import editContactReducer from "./store/reducers/editContactReducer";
import deleteContactReducer from "./store/reducers/deleteContactReducer";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
    addContact: addContactReducer,
    contactsList: contactsListReducer,
    currentContact: currentContactReducer,
    editContact: editContactReducer,
    deleteContact: deleteContactReducer,
});

const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunk)
));

const app = (
    <Provider store={store}>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
    </Provider>
)

ReactDOM.render(app, document.getElementById('root'));
